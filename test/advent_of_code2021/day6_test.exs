defmodule AdventOfCode2021.Day6Test do
  use ExUnit.Case, async: true

  test "day 6" do
    input = "3,4,3,1,2"
    {answer1, answer2} = AdventOfCode2021.Day6.solve(input)
    assert answer1 == 5934
    assert answer2 == 26984457539
  end
end
