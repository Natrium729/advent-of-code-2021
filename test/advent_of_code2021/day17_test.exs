defmodule AdventOfCode2021.Day17Test do
  use ExUnit.Case, async: true

  test "day 17" do
    input = "target area: x=20..30, y=-10..-5"
    {answer1, answer2} = AdventOfCode2021.Day17.solve(input)
    assert answer1 == 45
    assert answer2 == 0
  end
end
