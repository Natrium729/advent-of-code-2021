defmodule AdventOfCode2021.Day10Test do
  use ExUnit.Case, async: true

  test "day 10" do
    input = """
[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]
"""
    {answer1, answer2} = AdventOfCode2021.Day10.solve(input)
    assert answer1 == 26397
    assert answer2 == 288957
  end
end
