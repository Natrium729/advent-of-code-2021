defmodule AdventOfCode2021.Day14Test do
  use ExUnit.Case, async: true

  test "day 14" do
    input = """
NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C
"""
    {answer1, answer2} = AdventOfCode2021.Day14.solve(input)
    assert answer1 == 1588
    assert answer2 == 2188189693529
  end
end
