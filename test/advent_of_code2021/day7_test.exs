defmodule AdventOfCode2021.Day7Test do
  use ExUnit.Case, async: true

  test "day 7" do
    input = "16,1,2,0,4,2,7,1,2,14"
    {answer1, answer2} = AdventOfCode2021.Day7.solve(input)
    assert answer1 == 37
    assert answer2 == 168
  end
end
