defmodule AdventOfCode2021.Day1Test do
  use ExUnit.Case, async: true

  test "day 1" do
    input = """
    199
    200
    208
    210
    200
    207
    240
    269
    260
    263
    """
    {answer1, answer2} = AdventOfCode2021.Day1.solve(input)
    assert answer1 == 7
    assert answer2 == 5
  end
end
