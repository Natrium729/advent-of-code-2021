defmodule AdventOfCode2021.Day9Test do
  use ExUnit.Case, async: true

  test "day 9" do
    input = """
2199943210
3987894921
9856789892
8767896789
9899965678
"""
    {answer1, answer2} = AdventOfCode2021.Day9.solve(input)
    assert answer1 == 15
    assert answer2 == 1134
  end
end
