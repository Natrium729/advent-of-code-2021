defmodule AdventOfCode2021.Day16Test do
  use ExUnit.Case, async: true

  test "day 16" do
    input = "8A004A801A8002F478"
    {answer1, _} = AdventOfCode2021.Day16.solve(input)
    assert answer1 == 16

    input = "620080001611562C8802118E34"
    {answer1, _} = AdventOfCode2021.Day16.solve(input)
    assert answer1 == 12

    input = "C0015000016115A2E0802F182340"
    {answer1, _} = AdventOfCode2021.Day16.solve(input)
    assert answer1 == 23

    input = "A0016C880162017C3686B18A3D4780"
    {answer1, _} = AdventOfCode2021.Day16.solve(input)
    assert answer1 == 31

    input = "C200B40A82"
    {_, answer2} = AdventOfCode2021.Day16.solve(input)
    assert answer2 == 3

    input = "04005AC33890"
    {_, answer2} = AdventOfCode2021.Day16.solve(input)
    assert answer2 == 54

    input = "880086C3E88112"
    {_, answer2} = AdventOfCode2021.Day16.solve(input)
    assert answer2 == 7

    input = "CE00C43D881120"
    {_, answer2} = AdventOfCode2021.Day16.solve(input)
    assert answer2 == 9

    input = "D8005AC2A8F0"
    {_, answer2} = AdventOfCode2021.Day16.solve(input)
    assert answer2 == 1

    input = "F600BC2D8F"
    {_, answer2} = AdventOfCode2021.Day16.solve(input)
    assert answer2 == 0

    input = "9C005AC2F8F0"
    {_, answer2} = AdventOfCode2021.Day16.solve(input)
    assert answer2 == 0

    input = "9C0141080250320F1802104A08"
    {_, answer2} = AdventOfCode2021.Day16.solve(input)
    assert answer2 == 1
  end
end
