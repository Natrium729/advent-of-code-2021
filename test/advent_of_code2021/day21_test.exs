defmodule AdventOfCode2021.Day21Test do
  use ExUnit.Case, async: true

  test "day 21" do
    input = """
Player 1 starting position: 4
Player 2 starting position: 8
"""
    {answer1, _answer2} = AdventOfCode2021.Day21.solve(input)
    assert answer1 == 739785
    # assert answer2 == 444356092776315
  end
end
