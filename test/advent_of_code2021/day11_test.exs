defmodule AdventOfCode2021.Day11Test do
  use ExUnit.Case, async: true

  test "day 11" do
    input = """
5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526
"""
    {answer1, answer2} = AdventOfCode2021.Day11.solve(input)
    assert answer1 == 1656
    assert answer2 == 195
  end
end
