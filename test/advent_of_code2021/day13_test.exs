defmodule AdventOfCode2021.Day13Test do
  use ExUnit.Case, async: true

  test "day 13" do
    input = """
6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5
"""
    {answer1, answer2} = AdventOfCode2021.Day13.solve(input)
    assert answer1 == 17
    assert answer2 == "\n#####\n#   #\n#   #\n#   #\n#####"
  end
end
