defmodule AdventOfCode2021.Day2Test do
  use ExUnit.Case, async: true

  test "day 2" do
    input = """
    forward 5
    down 5
    forward 8
    up 3
    down 8
    forward 2
    """
    {answer1, answer2} = AdventOfCode2021.Day2.solve(input)
    assert answer1 == 150
    assert answer2 == 900
  end
end
