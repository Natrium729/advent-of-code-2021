defmodule AdventOfCode2021.Day3Test do
  use ExUnit.Case, async: true

  test "day 3" do
    input = """
    00100
    11110
    10110
    10111
    10101
    01111
    00111
    11100
    10000
    11001
    00010
    01010
    """
    {answer1, _} = AdventOfCode2021.Day3.solve(input)
    assert answer1 == 198
  end
end
