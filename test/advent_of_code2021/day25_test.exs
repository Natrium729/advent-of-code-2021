defmodule AdventOfCode2021.Day25Test do
  use ExUnit.Case, async: true

  test "day 25" do
    input = """
v...>>.vv>
.vv>>.vv..
>>.>v>...v
>>v>>.>.v.
v>v.vv.v..
>.>>..v...
.vv..>.>v.
v.v..>>v.v
....v..v.>
"""
    {answer1, _answer2} = AdventOfCode2021.Day25.solve(input)
    assert answer1 == 58
    # assert answer2 == 0
  end
end
