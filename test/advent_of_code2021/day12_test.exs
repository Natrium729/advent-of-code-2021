defmodule AdventOfCode2021.Day12Test do
  use ExUnit.Case, async: true

  test "day 12" do
    input = """
start-A
start-b
A-c
A-b
b-d
A-end
b-end
"""
    {answer1, answer2} = AdventOfCode2021.Day12.solve(input)
    assert answer1 == 10
    assert answer2 == 36

    input = """
dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc
"""
    {answer1, answer2} = AdventOfCode2021.Day12.solve(input)
    assert answer1 == 19
    assert answer2 == 103

    input = """
fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW
"""
        {answer1, answer2} = AdventOfCode2021.Day12.solve(input)
        assert answer1 == 226
        assert answer2 == 3509
  end
end
