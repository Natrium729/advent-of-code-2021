defmodule AdventOfCode2021.Day15Test do
  use ExUnit.Case, async: true

  test "day 15" do
    input = """
1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581
"""
    {answer1, _} = AdventOfCode2021.Day15.solve(input)
    assert answer1 == 40
    # assert answer2 == 315
  end
end
