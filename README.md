# Advent Of Code 2021

This is my participation to [Advent of Code 2021](https://adventofcode.com/2021), written in Elixir. I solved every day except for a few days where I solved only Part One, up to Day 17. From Day 18 onwards, I skipped most of the days.

My main objective was to learn the basics of Elixir, to see if the language suits me. (It does not, see below.) It's very likely I did things in a non efficient or idiomatic way.

To run, use

```
$ mix run -e "AdventOfCode2021.solve(1)"
```

where you replace `1` with the day for which you want to get the answers (or without argument if you want all the answers).

You can also run `mix test` to test the solutions against the examples given by Advent of Code.

My input files are in the `inputs` folder. The code assumes all inputs are valid (i.e. don't check if they are well-formed).

I haven't used OTP nor any agents/processes. I hadn't have the time to learn about them and they seemed overkill for problems in the kind of Advent of Code.

## My thoughts about Elixir

So what do I think about Elixir? I didn't really like my experience with it.

I found the language a bit verbose (all the `do end`!) and the error messages weren't that helpful (although I might have been spoilt by Rust in that aspect!).

It was also difficult to return to a dynamically-typed languages after using mainly Rust and TypeScript, but that issue is not inherent to Elixir.

I do love many aspects of functional programming (iterators and pattern matching amongst other), but stricter functional languages like Elixir are a bit to strict to my taste. Immutability is great, but sometimes some mutability makes the code easier to write and with less lines.

For example, while it may exist, I haven't found a way to increment a struct's field other that using the update syntax:

```elixir
natrium729 = %{natrium729 | natrium729.age + 1}
```

Contrast with a language with mutable variables:

```rust
natrium729.age = natrium729.age + 1
// Or even:
natrium729.age += 1
```

I also wished Elixir had real constant time arrays and not only linked lists. (It would seem Erlang has an array library and I could use that in Elixir? But it was Elixir that I was learning and didn't want to think too much about Erlang.)

I did like a few things, such as the aforementioned functional programming features. Bitstrings and binaries made parsing the input really easy, as well as solving some problems (notably day 16, my favourite). I liked the function capture syntax (`&my_fun(foo, &1, &2)`). Finally, I found interesting the concept of having `=` as a match operator instead of assignment, along with the pinning operator `^`.

But overall, Elixir didn't click for me. Maybe it's great when writing web servers and other real time applications with OTP and the Phoenix framework! But as a "regular" language, I wasn't that impressed and I didn't feel productive.
