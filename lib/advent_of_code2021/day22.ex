defmodule AdventOfCode2021.Day22 do
  defmodule Game do
    @enforce_keys [:pos_1, :pos_2]
    defstruct [:pos_1, :pos_2, score_1: 0, score_2: 0, next_player: 1, last_die: 0, rolled_count: 0]
    @type t :: %__MODULE__{
      pos_1: non_neg_integer,
      score_1: non_neg_integer,
      pos_2: non_neg_integer,
      score_2: non_neg_integer,
      next_player: 1 | 2,
      last_die: non_neg_integer,
      rolled_count: non_neg_integer,
    }
  end

  def solve(input) do
    _steps = parse_input(input)

    # It's too slow (see comment further down).
    # Well, we'll let it be.
    #
    # answer1 = follow_steps_naive(steps)
    # |> MapSet.size()
    answer1 = 0

    answer2 = 0

    {answer1, answer2}
  end

  defp parse_input(input) do
    String.split(input, "\n", trim: true)
    |> Enum.map(fn line ->
      [switch, coords] = String.split(line, " ", trim: true)
      [
        <<"x=", x_range::binary>>,
        <<"y=", y_range::binary>>,
        <<"z=", z_range::binary>>
      ] = String.split(coords, ",")
      x_range = String.split(x_range, "..")
      |> Enum.map(&String.to_integer/1)
      |> List.to_tuple()
      y_range = String.split(y_range, "..")
      |> Enum.map(&String.to_integer/1)
      |> List.to_tuple()
      z_range = String.split(z_range, "..")
      |> Enum.map(&String.to_integer/1)
      |> List.to_tuple()

      {String.to_atom(switch), x_range, y_range, z_range}
    end)
  end

  # Once again, I fall in the obvious trap:
  # it's too slow if we take account of all the cubes individually.
  # A solution would be to only count the intersections of the cuboid of each step
  # with the cubes already on.
  #
  # defp follow_steps_naive(steps) do
  #   Enum.reduce(steps, MapSet.new(), fn {switch, {x1, x2}, {y1, y2}, {z1, z2}}, acc ->
  #     for x <- x1..x2, y <- y1..y2, z <- z1..z2 do {x, y, z} end
  #     |> Stream.reject(fn {x, y, z} ->
  #       x < -50 or x > 50 or y < -50 or y > 50 or z < -50 or z > 50
  #     end)
  #     |> Enum.reduce(acc, fn {x, y, z}, acc ->
  #       if switch === :on do
  #         MapSet.put(acc, {x, y, z})
  #       else
  #         MapSet.delete(acc, {x, y, z})
  #       end
  #     end)
  #   end)
  # end
end
