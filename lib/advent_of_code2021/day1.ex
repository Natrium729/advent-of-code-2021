defmodule AdventOfCode2021.Day1 do
  def solve(input) do
    input = parse_input(input)
    {number_of_increases(input), number_of_window_increases(input)}
  end

  @spec parse_input(String.t) :: [integer]
  defp parse_input(input) do
    String.split(input)
    |> Enum.map(&String.to_integer/1)
  end

  defp number_of_increases(list) do
    Stream.chunk_every(list, 2, 1, :discard)
    |> Enum.count(fn [first, second] -> second - first > 0 end)
  end

  defp number_of_window_increases(list) do
    Stream.chunk_every(list, 3, 1, :discard)
    |> Stream.map(&Enum.sum/1)
    |> Stream.chunk_every(2, 1, :discard)
    |> Enum.count(fn [first, second] -> second - first > 0 end)
  end
end
