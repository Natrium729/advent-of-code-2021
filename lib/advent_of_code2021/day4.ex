defmodule AdventOfCode2021.Day4 do
  defmodule Board do
    defstruct [:numbers, winner: false]

    def mark(board, draw) do
      new_numbers = Enum.map(board.numbers, fn num ->
        if num === draw do
          :ok
        else
          num
        end
      end)

      rows = Stream.chunk_every(new_numbers, 5)
      columns = Stream.map(0..4, fn col ->
        Stream.drop(new_numbers, col)
        |> Enum.take_every(5) # There are 5 numbers in each columm.
      end)
      winner = Stream.concat(rows, columns)
      |> Enum.find(fn seq -> Enum.all?(seq, &(&1 === :ok)) end)

      %{board | numbers: new_numbers, winner: winner}
    end
  end

  def solve(input) do
    {numbers, boards} = parse_input(input)

    answer1 = solve1(numbers, boards)
    answer2 = solve2(numbers, boards)

    {answer1, answer2}
  end

  defp parse_input(input) do
    [numbers | boards] = String.split(input)

    numbers = String.split(numbers, ",")
    |> Enum.map(&String.to_integer/1)

    # Boards are 5 by 5, hence the chunks of 25.
    boards = Stream.map(boards, &String.to_integer/1)
    |> Stream.chunk_every(25)
    |> Enum.map(&%Board{numbers: &1})

    {numbers, boards}
  end

  defp solve1(numbers, boards) do
    Stream.scan(numbers, {0, boards}, fn num, {_, boards} ->
      new_boards = Enum.map(boards, &Board.mark(&1, num))
      {num, new_boards}
    end)
    |> Enum.find_value(fn {num, boards} ->
      winner = Enum.find(boards, &(&1.winner))
      if winner do
        sum = Stream.filter(winner.numbers, &(&1 !== :ok))
        |> Enum.sum()
        sum * num
      else
        nil
      end
    end)
  end

  defp solve2(numbers, boards) do
    Stream.scan(numbers, {0, boards}, fn num, {_, boards} ->
      new_boards = Enum.map(boards, &Board.mark(&1, num))
      if Enum.count_until(new_boards, 2) !== 1 do # We haven't reached the last board.
        new_boards = Enum.reject(new_boards, &(&1.winner))
        {num, new_boards}
      else
        {num, new_boards}
      end
    end)
    |> Enum.find_value(fn {num, boards} ->
      if Enum.count_until(boards, 2) === 1 && hd(boards).winner do
        sum = Stream.filter(hd(boards).numbers, &(&1 !== :ok))
        |> Enum.sum()
        sum * num
      else
        nil
      end
    end)
  end
end
