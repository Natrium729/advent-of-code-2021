defmodule AdventOfCode2021.Day7 do
  def solve(input) do
    positions = parse_input(input)

    # For Part One, my intuition was that the optimal destination is the median of the current positions.
    # (If we get a non-round number, we try the floor and the ceil.)
    # While I'm not sure it's always the case, it gives the right answer for my input.

    len = length(positions)
    potential_destinations = if rem(len, 2) === 0 do
      mid_i = floor(len / 2)
      mid_points_sum = Enum.slice(positions, mid_i - 1 .. mid_i)
      |> Enum.sum()
      if rem(mid_points_sum, 2) === 0 do
        [floor(mid_points_sum / 2)] # Floor to convert the float to an integer.
      else
        average = mid_points_sum / 2
        [floor(average), ceil(average)]
      end
    else
      [Enum.at(positions, floor(len / 2))]
    end

    answer1 = Stream.map(potential_destinations, fn dest ->
      Stream.map(positions, &fuel_needed(&1, dest))
      |> Enum.sum()
    end)
    |> Enum.min()

    pos_sum = Enum.sum(positions)
    potential_destinations = if rem(pos_sum, len) === 0 do
      [floor(pos_sum / len)]
    else
      average = pos_sum / len
      [floor(average), ceil(average)]
    end

    # For Part Two, my intuition was that the optimal destination is the average of the current positions.
    # (If we get a non-round number, we try the floor and the ceil.)
    # While I'm not sure it's always the case, it gives the right answer for my input.

    answer2 = Stream.map(potential_destinations, fn dest ->
      Stream.map(positions, &fuel_needed_2(&1, dest))
      |> Enum.sum()
    end)
    |> Enum.min()

    {answer1, answer2}
  end

  defp parse_input(input) do
    String.split(input, ",")
    |> Stream.map(&String.to_integer(String.trim(&1)))
    |> Enum.sort()
  end

  defp fuel_needed(pos, dest) do
    abs(dest - pos)
  end

  defp fuel_needed_2(pos, dest) do
    # I forgot I could use the formula n * (n + 1) * 0.5.
    # Well, I think I'll keep the naive solution since it works
    # and it's the first thing I thought of.
    Enum.sum(1 .. abs(dest - pos))
  end
end
