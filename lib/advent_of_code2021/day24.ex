defmodule AdventOfCode2021.Day24 do
  defmodule VM do
    defstruct [w: 0, x: 0, y: 0, z: 0]
    @type opcode :: {atom, atom, atom | integer}
    @type t :: %__MODULE__{
      w: integer,
      x: integer,
      y: integer,
      z: integer,
    }

    def new() do
      %VM{}
    end

    defp operand_value(vm, var) do
      if is_atom(var) do
        Map.get(vm, var)
      else
        var
      end
    end

    # When all opcodes has been executed, return the VM.
    def run(vm, [], _inputs) do
      vm
    end

    def run(vm, [opcode | rest], inputs) do
      case opcode do
        {:inp, a} ->
          # We'll assume there are still digits left.
          [next_input | inputs] = inputs
          vm = Map.put(vm, a, next_input)
          run(vm, rest, inputs)
        {:add, a, b} ->
          vm = Map.put(vm, a, operand_value(vm, a) + operand_value(vm, b))
          run(vm, rest, inputs)
        {:mul, a, b} ->
          vm = Map.put(vm, a, operand_value(vm, a) * operand_value(vm, b))
          run(vm, rest, inputs)
        {:div, _, 0} ->
          :error
        {:div, a, b} ->
          vm = Map.put(vm, a, div(operand_value(vm, a), operand_value(vm, b)))
          run(vm, rest, inputs)
        {:mod, a, b} when a < 0 or b <= 0 ->
          :error
        {:mod, a, b} ->
          vm = Map.put(vm, a, rem(operand_value(vm, a), operand_value(vm, b)))
          run(vm, rest, inputs)
        {:eql, a, b} ->
          result = if operand_value(vm, a) === operand_value(vm, b) do
            1
          else
            0
          end
          vm = Map.put(vm, a, result)
          run(vm, rest, inputs)
      end
    end
  end

  def solve(input) do
    _opcodes = parse_input(input)

    # Obviously, trying to find the biggest possible solution by brute force is too long.
    # Well, I guess it was worth a try? :)
    #
    # answer1 = Stream.map(99999999999999..11111111111111, &Integer.digits/1)
    # |> Stream.reject(&(0 in &1))
    # |> Stream.map(fn digits ->
    #   vm = VM.new()
    #   VM.run(vm, opcodes, digits)
    # end)
    # |> Enum.find_value(fn vm ->
    #   if vm !== :error and vm.z === 0 do
    #     vm.z
    #   else
    #     nil
    #   end
    # end)
    answer1 = 0

    answer2 = 0

    {answer1, answer2}
  end

  defp parse_input(input) do
    String.split(input, "\n", trim: true)
    |> Enum.map(fn line ->
      String.split(line, " ", trim: true)
      |> Enum.map(fn val ->
        case Integer.parse(val) do
          {num, _} -> num
          :error -> String.to_existing_atom(val)
        end
      end)
      |> List.to_tuple()
    end)
  end
end
