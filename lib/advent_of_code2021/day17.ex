defmodule AdventOfCode2021.Day17 do
  def solve(input) do
    {_x_min, _x_max, y_min, _y_max}  = parse_input(input)

    # Since the vertical and horizontal movements are independant,
    # we can for the first anwser assume the target area is directly below the submarine.
    #
    # If launching the probe upward at a speed v0,
    # then it will have a speed of -(v0 + 1) when it comes back to the altitude of 0.
    #
    # Therefore, the problem becomes:
    # What's the highest speed possible when launching the probe downward?
    #
    # The answer is:
    # the speed that makes it reach the lowest point of the target in one step.
    #
    # Then going back to the initial problem,
    # we find that v0 when launching upward should be abs(speed - 1)
    # if we want the probe to go the highest.
    #
    # To get the maximum altitude, we sum the speed at each step until 0.
    # For example, 10 + 9 + ... + 1 + 0.
    # Or, with the formula: speed * (speed + 1) * 0.5.

    y_speed = abs(y_min) - 1
    max_height = y_speed * (y_speed + 1) * 0.5
    answer1 = max_height

    answer2 = 0

    {answer1, answer2}
  end

  defp parse_input(input) do
    <<"target area: ", coords::binary>> = String.trim(input)
    [x_coords, y_coords] = String.split(coords, ", ")

    <<"x=", x_range::binary>> = x_coords
    [x_min, x_max] = String.split(x_range, "..")
    |> Stream.map(&String.to_integer/1)
    |> Enum.sort()

    <<"y=", y_range::binary>> = y_coords
    [y_min, y_max] = String.split(y_range, "..")
    |> Stream.map(&String.to_integer/1)
    |> Enum.sort()

    {x_min, x_max, y_min, y_max}
  end
end
