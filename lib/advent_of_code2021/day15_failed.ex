# This file contains a first attempt to solve the day 15 myself.
# It's too slow, however, so I just used Dijkstra's algorithm in the end.
# (See day15.ex for it.)

defmodule AdventOfCode2021.Day15Failed do
  defmodule Grid do
    defstruct [:cells, :height, :width]
    @type t :: %__MODULE__{
      cells: %{non_neg_integer => non_neg_integer},
      height: non_neg_integer,
      width: non_neg_integer
    }

    @spec new(%{non_neg_integer => non_neg_integer}, non_neg_integer, non_neg_integer) :: Grid.t
    def new(cells, height, width) do
      %Grid{cells: cells, height: height, width: width}
    end

    @spec get_at(Grid.t, non_neg_integer, non_neg_integer) :: non_neg_integer
    def get_at(grid, x, y) do
      if x < 0 or x > grid.width - 1 or y < 0 or y > grid.height - 1 do
        nil
      else
        i = x + y * grid.width
        grid.cells[i]
      end
    end

    @spec neighbours(Grid.t, non_neg_integer, non_neg_integer) :: [{non_neg_integer, non_neg_integer, non_neg_integer}]
    def neighbours(grid, x, y) do
      [
        {0, -1}, # Top.
        {0, 1}, # Bottom.
        {-1, 0}, # Left.
        {1, 0}, # Right.
      ]
      |> Stream.map(fn {dx, dy} ->
        {x + dx, y + dy, get_at(grid, x + dx, y + dy)}
      end)
      |> Enum.filter(fn {_, _, cell} -> cell !== nil end)
    end
  end

  def solve(input) do
    grid = parse_input(input)

    paths = find_paths(grid)

    answer1 = Stream.map(paths, fn [{_, _, risk} | _] -> risk end)
    |> Enum.min()

    answer2 = 0

    {answer1, answer2}
  end

  defp parse_input(input) do
    lines = String.split(input, "\n", trim: true)
    height = length(lines)
    width = length(String.to_charlist(hd(lines)))

    cells = Stream.flat_map(lines, fn line ->
      Enum.map(String.codepoints(line), &String.to_integer/1)
    end)
    |> Stream.zip(0 .. height * width - 1)
    |> Enum.reduce(%{}, fn {val, i}, acc -> Map.put(acc, i, val) end)

    Grid.new(cells, height, width)
  end

  defp find_paths(grid) do
    # The theoretical minimum risk is if all the cells on the path are 1.
    # We start from this minimum. If no path can reach the end under this number,
    # We increase it and start back where we stopped.
    Stream.iterate(grid.width + grid.height - 2, &(&1 + 1))
    # We start from the end because each step will be prepended to the list of steps.
    # Like that, we get the paths in the right order.)
    |> Stream.scan([[{grid.width - 1, grid.height - 1, 0}]], fn lim, starts ->
      Enum.flat_map(starts, fn start -> find_paths(grid, start, lim) end)
    end)
    |> Enum.find_value(fn paths ->
      completed_paths = Enum.filter(paths, fn [{x, y, _} | _] -> {x, y} === {0, 0} end)
      if Enum.count_until(completed_paths, 1) === 1 do
        completed_paths
      else
        nil
      end
    end)
  end

  defp find_paths(grid, so_far, risk_limit) do
    [{last_x, last_y, risk_so_far} | _] = so_far
    Grid.neighbours(grid, last_x, last_y)
    |> Enum.filter(fn {nb_x, nb_y, _} ->
      Enum.find(so_far, fn {x, y, _} -> {nb_x, nb_y} === {x, y} end) === nil
    end)
    |> Enum.flat_map(fn {x, y, risk} ->
      cond do
        # We reached the end of the journey, return this particular path.
        # (Wrapped in a list because of the flat_map.)
        {x, y} === {0, 0} ->
          [[{x, y, risk_so_far + risk} | so_far]]
        # We exceeded the risk limit, stop now and return this particular path.
        risk_so_far + risk > risk_limit ->
          [[{x, y, risk_so_far + risk} | so_far]]
        # Else we continue the path.
        true ->
          find_paths(grid, [{x, y, risk + risk_so_far} | so_far], risk_limit)
      end
    end)
  end
end
