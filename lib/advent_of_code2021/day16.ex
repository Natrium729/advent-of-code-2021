defmodule AdventOfCode2021.Day16 do
  def solve(input) do
    transmission = parse_input(input)

    # The rest should always be a sequence of zeros, if any.
    {packet, _} = parse_packet(transmission)

    answer1 = sum_versions(packet)

    answer2 = evaluate_packet(packet)

    {answer1, answer2}
  end

  defp parse_input(input) do
    String.trim(input)
    |> String.codepoints()
    |> Stream.map(&String.to_integer(&1, 16))
    |> Enum.reduce(<<>>, fn nibble, acc ->
      <<acc::bitstring, nibble::4>>
    end)
  end

  defp sum_versions(packet) do
    if Map.has_key?(packet, :subpackets) do
      subpacket_sum = Stream.map(packet.subpackets, &sum_versions/1)
      |> Enum.sum()
      packet.version + subpacket_sum
    else
      packet.version
    end
  end

  defp evaluate_packet(packet) do
    if packet.type_id === 4 do
      packet.litteral
    else
      subpackets_values = Enum.map(packet.subpackets, &evaluate_packet/1)
      case packet.type_id do
        0 -> Enum.sum(subpackets_values)
        1 -> Enum.product(subpackets_values)
        2 -> Enum.min(subpackets_values)
        3 -> Enum.max(subpackets_values)
        5 ->
          [a, b | _] = subpackets_values
          if a > b, do: 1, else: 0
        6 ->
          [a, b | _] = subpackets_values
          if a < b, do: 1, else: 0
        7 ->
          [a, b | _] = subpackets_values
          if a === b, do: 1, else: 0
      end
    end
  end

  defp parse_packet(bits) do
    {packet, rest} = get_version({%{}, bits})
    |> get_type_id()
    {packet, rest} = case packet.type_id do
      4 -> get_litteral_value({packet, rest})
      _ ->
        get_subpackets({packet, rest})
    end
    {packet, rest}
  end

  defp get_version({packet, bits}) do
    <<version::3, rest::bitstring>> = bits
    {Map.put(packet, :version, version), rest}
  end

  defp get_type_id({packet, bits}) do
    <<id::3, rest::bitstring>> = bits
    {Map.put(packet, :type_id, id), rest}
  end

  defp get_litteral_value({packet, bits}) do
    {value, rest} = parse_litteral(bits)
    {Map.put(packet, :litteral, value), rest}
  end

  defp parse_litteral(bits, so_far \\ <<>>) do
    <<prefix::1, value::4, rest::bitstring>> = bits
    case prefix do
      1 -> parse_litteral(rest, <<so_far::bitstring, value::4>>)
      0 ->
        # We get the bits and join them into a string, then convert it into a number.
        # It would surely be more performant to convert it ourselves without the intermediate string.
        value = <<so_far::bitstring, value::4>>
        value = for <<bit::1 <- value>>, into: "" do
          if bit === 0, do: "0", else: "1"
        end
        |> String.to_integer(2)
        {value, rest}
    end
  end

  defp get_subpackets({packet, bits}) do
    <<id::1, rest::bitstring>> = bits
    {subpackets, rest} = if id === 0 do # Length in bits in next 15 bits.
      <<l::15, rest::bitstring>> = rest
      # Apparently it's not possible to take a variable number of bits from a bitstring,
      # So we'll take one at a time.
      {sub_bits, rest} = Enum.reduce(1..l, {<<>>, rest}, fn _, {sub_bits, rest} ->
          <<next_bit::1, rest::bitstring>> = rest
          {<<sub_bits::bitstring, next_bit::1>>, rest}
        end)
      {get_subpackets_0(sub_bits), rest}
    else # Number of packets in next 11 bits.
      <<n::11, rest::bitstring>> = rest
      Enum.reduce(1..n, {[], rest}, fn _, {subpackets, rest} ->
        {next, rest} = parse_packet(rest)
        {[next | subpackets], rest}
      end)
    end
    # We reverse the order of the subpackets because we pushed then at the front.
    subpackets = Enum.reverse(subpackets)
    {Map.put(packet, :subpackets, subpackets), rest}
  end

  defp get_subpackets_0(bits) do
    get_subpackets_0(bits, [])
  end

  defp get_subpackets_0(<<>>, so_far) do
    so_far
  end

  defp get_subpackets_0(bits, so_far) do
    {packet, rest} = parse_packet(bits)
    get_subpackets_0(rest, [packet | so_far])
  end
end
