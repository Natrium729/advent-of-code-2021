defmodule AdventOfCode2021.Day14 do
  def solve(input) do
    {template, rules} = parse_input(input)

    # For Part One, we use the naive way.

    step_10_freq = steps_naive(template, rules)
    |> Enum.at(10)
    |> Enum.frequencies()

    {min, max} = Enum.min_max(Map.values(step_10_freq))

    answer1 = max - min

    # For part 2, we use the more clever way.

    step_40_freq = steps(template, rules)
    |> Enum.at(40)
    |> Enum.reduce(%{}, fn {pair, freq}, acc ->
      Map.update(acc, elem(pair, 0), freq, &(&1 + freq))
    end)

    {min, max} = Enum.min_max(Map.values(step_40_freq))

    answer2 = max - min

    {answer1, answer2}
  end

  defp parse_input(input) do
    [template, rules] = String.split(input, "\n\n", trim: true)

    template = String.to_charlist(template)

    rules = for line <- String.split(rules, "\n", trim: true), into: %{} do
      [pair, insertion] = String.split(line, " -> ", trim: true)
      [a, b] = String.to_charlist(pair)
      [c] = String.to_charlist(insertion)
      {{a, b}, c}
    end

    {template, rules}
  end

  # The naive way: Use a list containing all the elements of the polymer.

  defp follow_rules_naive(current, rules) do
    Stream.chunk_every(current, 2, 1)
    |> Stream.flat_map(fn
      [a, b] -> [a, rules[{a, b}]]
      [a] -> [a] # It's the last element, leave it.
    end)
  end

  defp steps_naive(template, rules) do
    Stream.iterate(template, fn current -> follow_rules_naive(current, rules) end)
  end

  # The more clever way: Use a list containing the frequencies of each pair.

  defp follow_rules(pairs, rules) do
    Enum.reduce(pairs, pairs, fn {pair, freq}, acc ->
      case pair do
        {_} -> acc # It's the last element, do nothing.
        {a, b} ->
          new = rules[pair]
          # Decrease the current pair.
          acc = Map.update(acc, {a, b}, 0, &(&1 - freq))
          # Increase the two created pairs.
          acc = Map.update(acc, {a, new}, freq, &(&1 + freq))
          Map.update(acc, {new, b}, freq, &(&1 + freq))
      end
    end)
  end

  defp steps(template, rules) do
    Stream.chunk_every(template, 2, 1)
    |> Stream.map(&List.to_tuple/1)
    |> Enum.frequencies()
    |> Stream.iterate(fn current -> follow_rules(current, rules) end)
  end
end
