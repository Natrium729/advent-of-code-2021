defmodule AdventOfCode2021.Day6 do
  def solve(input) do
    fish = parse_input(input)

    answer1 = Enum.reduce(0..79, fish, fn _, acc -> step(acc) end)
    |> Map.values()
    |> Enum.sum()

    answer2 = Enum.reduce(0..255, fish, fn _, acc -> step(acc) end)
    |> Map.values()
    |> Enum.sum()

    {answer1, answer2}
  end

  defp parse_input(input) do
    String.split(input, ",")
    |> Stream.map(&String.to_integer(String.trim(&1)))
    |> Enum.frequencies()
  end

  defp step(fish) do
    Enum.reduce(fish, fish, fn {val, freq}, acc ->
      acc = Map.put(acc, val, Map.get(acc, val, freq) - freq)
      next_val = if val === 0, do: 6, else: val - 1
      acc = Map.put(acc, next_val, Map.get(acc, next_val, 0) + freq)
      if val === 0 do
        Map.put(acc, 8, Map.get(acc, 8, 0) + freq)
      else
        acc
      end
    end)
  end
end
