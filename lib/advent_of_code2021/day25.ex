defmodule AdventOfCode2021.Day25 do
  defmodule Cucumbers do
    @enforce_keys [:cells, :height, :width]
    defstruct [:cells, :height, :width]
    @type t :: %__MODULE__{
      cells: %{{non_neg_integer, non_neg_integer} => ?v | ?>},
      height: non_neg_integer,
      width: non_neg_integer,
    }

    def new(input) do
      lines = String.split(input, "\n", trim: true)
      height = length(lines)
      width = hd(lines) |> String.to_charlist() |> length()
      cells = for y <- 0 .. height - 1, x <- 0 .. width - 1 do {x, y} end
      |> Stream.zip(Stream.flat_map(lines, fn line ->
        String.to_charlist(line)
      end))
      |> Stream.reject(fn {_, cell} -> cell == ?. end)
      |> Enum.reduce(%{}, fn {{x, y}, cell}, acc ->
        Map.put(acc, {x, y}, cell)
      end)

      %Cucumbers{cells: cells, height: height, width: width}
    end

    defp east_cell_free?(cucumbers, x, y) do
      xx = rem(x + 1, cucumbers.width)
      not Map.has_key?(cucumbers.cells, {xx, y})
    end

    defp south_cell_free?(cucumbers, x, y) do
      yy = rem(y + 1, cucumbers.height)
      xx = rem(x + cucumbers.width - 1, cucumbers.width)
      # The cell south is free, but with no east-facing cell to the west
      # (that will move to the south cell)
      # or occupied by and east-facing whose east cell is free
      # (because that east-facing cucumber will move first)
      (
        not Map.has_key?(cucumbers.cells, {x, yy}) and
        Map.get(cucumbers.cells, {xx, yy}, ?.) !== ?>
      ) or (
        cucumbers.cells[{x, yy}] == ?> and
        east_cell_free?(cucumbers, x, yy)
      )
    end

    @spec next_step(Cucumbers.t) :: Cucumbers.t
    def next_step(cucumbers) do
      new_cells = Enum.reduce(cucumbers.cells, %{}, fn {{x, y}, cell}, acc ->
        cond do
          cell === ?> and east_cell_free?(cucumbers, x, y) ->
            Map.put(acc, {rem(x + 1, cucumbers.width), y}, ?>)
          cell === ?v and south_cell_free?(cucumbers, x, y) ->
            Map.put(acc, {x, rem(y + 1, cucumbers.height)}, ?v)
          true ->
            Map.put(acc, {x, y}, cell)
        end
      end)
      %{cucumbers | cells: new_cells}
    end

    @spec steps(Cucumbers.t) :: any
    def steps(cucumbers) do
      Stream.iterate(cucumbers, &next_step/1)
    end

    @spec to_string(Cucumbers.t) :: String.t
    def to_string(cucumbers) do
      for y <- 0 .. cucumbers.height - 1, x <- 0 .. cucumbers.width - 1 do {x, y} end
      |> Enum.reduce("", fn {x, y}, acc ->
        acc = if x === 0 and y !== 0 do
          acc <> "\n"
        else
          acc
        end
        case Map.get(cucumbers.cells, {x, y}, ?.) do
          ?> -> acc <> ">"
          ?v -> acc <> "v"
          ?. -> acc <> "."
        end
      end)
    end
  end

  def solve(input) do
    cucumbers = parse_input(input)

    indices = Stream.iterate(0, &(&1 + 1))
    answer1 = Cucumbers.steps(cucumbers)
    |> Stream.chunk_every(2, 1)
    |> Stream.zip(indices)
    |> Enum.find_value(fn {[curr, next], i} ->
      if next.cells == curr.cells do
        i + 1
      else
        nil
      end
    end)

    answer2 = 0

    {answer1, answer2}
  end

  defp parse_input(input) do
    Cucumbers.new(input)
  end
end
