defmodule AdventOfCode2021.Day3 do
  use Bitwise, only_operators: true

  def solve(input) do
    input = parse_input(input)
    n_input = length(input)
    n_bit = length(hd(input))

    {gamma_rate, epsilon_rate} = Stream.zip(input)
    |> Stream.map(fn bits ->
      sum = Tuple.sum(bits)
      if sum > n_input / 2 do
        {1, 0}
      else
        {0, 1}
      end
    end)
    |> Stream.with_index()
    |> Enum.reduce({0, 0}, fn {{bit_g, bit_e}, i}, {acc_g, acc_e} ->
      {
        acc_g + (bit_g <<< (n_bit - i - 1)),
        acc_e + (bit_e <<< (n_bit - i - 1))
      }
    end)


    {gamma_rate * epsilon_rate, 0}
  end

  defp parse_input(input) do
    String.split(input)
    |> Stream.map(&String.codepoints()/1)
    |> Enum.map(fn char_list -> Enum.map(char_list, &String.to_integer/1) end)
  end
end
