defmodule AdventOfCode2021.Day15 do
  defmodule Grid do
    defstruct [:cells, :height, :width]
    @type t :: %__MODULE__{
      cells: %{non_neg_integer => non_neg_integer},
      height: non_neg_integer,
      width: non_neg_integer
    }

    @spec new(%{non_neg_integer => non_neg_integer}, non_neg_integer, non_neg_integer) :: Grid.t
    def new(cells, height, width) do
      %Grid{cells: cells, height: height, width: width}
    end

    @spec get_at(Grid.t, non_neg_integer, non_neg_integer, boolean) :: non_neg_integer
    def get_at(grid, x, y, false) do
      if x < 0 or x > grid.width - 1 or y < 0 or y > grid.height - 1 do
        nil
      else
        i = x + y * grid.width
        grid.cells[i]
      end
    end

    # When the grid is 5 times larger.
    # We actually don't store the whole grid,
    # we calculate out-of-bounds value according to the rules.
    def get_at(grid, x, y, true) do
      if x < 0 or x > grid.width * 5 - 1 or y < 0 or y > grid.height * 5 - 1 do
        nil
      else
        i = rem(x, grid.width) + rem(y, grid.height) * grid.width
        cell = grid.cells[i]
        x_shift = div(x, grid.width)
        y_shift = div(y, grid.height)
        rem(cell + x_shift + y_shift - 1, 9) + 1
      end
    end

    @spec neighbours(Grid.t, non_neg_integer, non_neg_integer, boolean) :: [{non_neg_integer, non_neg_integer, non_neg_integer}]
    def neighbours(grid, x, y, extended) do
      [
        {0, -1}, # Top.
        {0, 1}, # Bottom.
        {-1, 0}, # Left.
        {1, 0}, # Right.
      ]
      |> Stream.map(fn {dx, dy} ->
        {x + dx, y + dy, get_at(grid, x + dx, y + dy, extended)}
      end)
      |> Enum.filter(fn {_, _, cell} -> cell !== nil end)
    end
  end

  def solve(input) do
    grid = parse_input(input)

    answer1 = find_shortest_path(grid)

    # Unless I've implemented it in a really unoptimised way,
    # Dijkstra's algorithm is too slow for the 5 times bigger cave.
    # Or maybe there's a trick and you don't need to run it for the whole cave?
    # answer2 = find_shortest_path(grid, true)
    answer2 = 0

    {answer1, answer2}
  end

  defp parse_input(input) do
    lines = String.split(input, "\n", trim: true)
    height = length(lines)
    width = length(String.to_charlist(hd(lines)))

    cells = Stream.flat_map(lines, fn line ->
      Enum.map(String.codepoints(line), &String.to_integer/1)
    end)
    |> Stream.zip(0 .. height * width - 1)
    |> Enum.reduce(%{}, fn {val, i}, acc -> Map.put(acc, i, val) end)

    Grid.new(cells, height, width)
  end

  # That's Dijkstra's algorithm.
  defp find_shortest_path(grid, extended \\ false) do
    x_max = if extended do
      grid.width * 5 - 1
    else
      grid.width - 1
    end
    y_max = if extended do
      grid.height * 5 - 1
    else
      grid.height - 1
    end
    unvisited = for x <- 0..x_max, y <- 0..y_max, {x, y} !== {0, 0}, into: %{} do
      {{x, y}, Grid.get_at(grid, x, y, extended)}
    end
    distances = %{{0, 0} => 0}
    Stream.iterate({{0, 0}, unvisited, distances}, fn {{x, y}, unvisited, distances} ->
      current_distance = Map.get(distances, {x, y})
      neighbours = Grid.neighbours(grid, x, y, extended)
      distances = Enum.reduce(neighbours, distances, fn {nx, ny, nd}, acc ->
        Map.update(acc, {nx, ny}, current_distance + nd, fn val ->
          min(current_distance + nd, val)
        end)
      end)
      unvisited = Map.delete(unvisited, {x, y})
      next = Map.keys(unvisited)
      |> Stream.filter(&Map.has_key?(distances, &1))
      |> Enum.min_by(&Map.get(distances, &1))
      {next, unvisited, distances}
    end)
    |> Enum.find_value(fn {{x, y}, _, distances} ->
      if {x, y} === {x_max, y_max} do
        distances[{x, y}]
      else
        nil
      end
    end)
  end
end
