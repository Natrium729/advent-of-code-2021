defmodule AdventOfCode2021.Day8 do
  def solve(input) do
    data = parse_input(input)

    answer1 = Stream.flat_map(data, fn {_, output} ->
      Stream.filter(output, &represents_unique_number?/1)
    end)
    |> Enum.count()

    answer2 = Stream.map(data, &decode_entry/1)
    |> Enum.sum()

    {answer1, answer2}
  end

  defp parse_input(input) do
    # While binary numbers would likely be more efficient to store the segments,
    # it's easier to wrap one's head around sets of letters.
    String.split(input, "\n", trim: true)
    |> Stream.map(&String.split(&1, " | "))
    |> Enum.map(fn [patterns, output] ->
      patterns = String.split(patterns)
      |> Enum.map(fn x -> String.to_charlist(x) |> MapSet.new() end)
      output = String.split(output)
      |> Enum.map(fn x -> String.to_charlist(x) |> MapSet.new() end)
      {patterns, output}
    end)
  end


  # Returns true if the given set of segments represents a unique number.
  # For example, A 2-segment set can only represent the number 1.
  defp represents_unique_number?(set) do
    s = MapSet.size(set)
    s === 2 or s === 4 or s === 3 or s === 7
  end

  defp decode_entry({patterns, output}) do
    # We'll never determine which signal wire maps to which segment.
    # Instead, we'll directly find which combination represents which number
    # with well chosen intersections.

    # First, the numbers which use a unique number of segments.
    uniques = Enum.filter(patterns, &represents_unique_number?/1)
    numbers = Enum.reduce(uniques, %{}, fn unique, acc ->
      case MapSet.size(unique) do
        2 -> Map.put(acc, 1, unique)
        4 -> Map.put(acc, 4, unique)
        3 -> Map.put(acc, 7, unique)
        7 -> Map.put(acc, 8, unique)
      end
    end)
    # Then the others. Some of these functions are order-dependant.
    # (i.e. They need some other numbers to be found first.)
    |> find_number_6(patterns)
    |> find_number_3(patterns)
    |> find_number_5(patterns)
    |> find_number_2(patterns)
    |> find_number_9(patterns)
    |> find_number_0(patterns)

    output_length = length(output)
    Enum.zip(output_length - 1 .. 0, output)
    |> Enum.reduce(0, fn {e, digit_segments}, acc ->
      # We could have made the map 2-way,
      # which would have been more performant,
      # But finding linearly is good enough.
      digit = Enum.find_value(numbers, fn {k, v} ->
        if v == digit_segments do
          k
        else
          nil
        end
      end)
      acc + digit * Integer.pow(10, e)
    end)

    # # And now, as an eternal reminder that you have to think before writing code,
    # # here's my first, terrible and complicated attempt
    # # to find which signal wire maps to which segment.
    #
    # x = Enum.reduce(uniques, segment_mapping, fn segments, acc ->
    #   corresponding_number = case MapSet.size(segments) do
    #     2 -> 1
    #     4 -> 4
    #     3 -> 7
    #     7 -> 8
    #   end
    #   target_segments = Map.get(number_segments, corresponding_number)
    #   Enum.reduce(segments, acc, fn source_segment, acc ->
    #     current_targets = Map.get(acc, source_segment)
    #     Map.put(acc, source_segment, MapSet.intersection(current_targets, target_segments))
    #   end)
    # end)
    #
    # IO.inspect(dedup_mapping(x))
  end

  defp find_number_6(numbers, patterns) do
    # 6 is the only number written with six segments
    # whose intersection with 1 has one segment.
    six_segments = Stream.filter(patterns, &(MapSet.size(&1) === 6))
    six = Enum.find(six_segments, fn segments ->
      intersection = MapSet.intersection(segments, numbers[1])
      MapSet.size(intersection) === 1
    end)
    Map.put(numbers, 6, six)
  end

  defp find_number_3(numbers, patterns) do
    # 3 is the only number written with five segments
    # whose intersection with 1 has two segments.
    five_segments = Stream.filter(patterns, &(MapSet.size(&1) === 5))
    three = Enum.find(five_segments, fn segments ->
      intersection = MapSet.intersection(segments, numbers[1])
      MapSet.size(intersection) === 2
    end)
    Map.put(numbers, 3, three)
  end

  defp find_number_5(numbers, patterns) do
    # 5 is the only number written with five segments, other than 3,
    # whose intersection with 4 has three segments.
    three = numbers[3]
    five_segments = Stream.filter(patterns, fn segments ->
      MapSet.size(segments) === 5 and segments != three
    end)
    five = Enum.find(five_segments, fn segments ->
      intersection = MapSet.intersection(segments, numbers[4])
      MapSet.size(intersection) === 3
    end)
    Map.put(numbers, 5, five)
  end

  defp find_number_2(numbers, patterns) do
    # 2 is the only number written with five segments
    # whose intersection with 4 has two segments.
    five_segments = Stream.filter(patterns, &(MapSet.size(&1) === 5))
    two = Enum.find(five_segments, fn segments ->
      intersection = MapSet.intersection(segments, numbers[4])
      MapSet.size(intersection) === 2
    end)
    Map.put(numbers, 2, two)
  end

  defp find_number_9(numbers, patterns) do
    # 9 is the only number written with six segments
    # whose intersection with 4 has four segments.
    six_segments = Stream.filter(patterns, &(MapSet.size(&1) === 6))
    nine = Enum.find(six_segments, fn segments ->
      intersection = MapSet.intersection(segments, numbers[4])
      MapSet.size(intersection) === 4
    end)
    Map.put(numbers, 9, nine)
  end

  defp find_number_0(numbers, patterns) do
    # 0 is the only number written with six segments, other than 6,
    # whose intersection with 4 has three segments.
    six = numbers[6]
    six_segments = Stream.filter(patterns, fn segments ->
      MapSet.size(segments) === 6 and segments != six
    end)
    zero = Enum.find(six_segments, fn segments ->
      intersection = MapSet.intersection(segments, numbers[4])
      MapSet.size(intersection) === 3
    end)
    Map.put(numbers, 0, zero)
  end

  # # And here, another eternal reminder that you have to think before writing code.
  #
  # defp dedup_mapping(mapping) do
  #   found = Enum.filter(Map.values(mapping), fn v -> MapSet.size(v) === 1 end)
  #   new_mapping = Enum.reduce(mapping, %{}, fn {k, v}, acc ->
  #     deduped = Enum.reduce(found, v, &MapSet.delete(&2, &1))
  #     Map.put(acc, k, deduped)
  #   end)
  #
  #   if new_mapping == mapping do
  #     mapping
  #   else
  #     dedup_mapping(new_mapping)
  #   end
  # end
end
