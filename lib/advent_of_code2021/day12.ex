defmodule AdventOfCode2021.Day12 do
  def solve(input) do
    graph = parse_input(input)

    answer1 = length(find_paths(graph))
    answer2 = length(find_paths_2(graph))

    {answer1, answer2}
  end

  defp parse_input(input) do
    String.split(input, "\n", trim: true)
    |> Enum.reduce(%{}, fn line, acc ->
      [first, second] = String.split(line, "-")
      add_to_graph(acc, first, second)
      |> add_to_graph(second, first)
    end)
  end

  defp add_to_graph(map, from, to) do
    Map.update(map, from, MapSet.new([to]), &MapSet.put(&1, to))
  end

  defp small_cave?(cave) do
    # Elixir doesn't have a `lowercase?` function. :(
    String.downcase(cave) === cave
  end

  defp find_paths(graph) do
    # We start from the end because each step will be prepended to the list of steps.
    # Like that, we get the paths in the right order.
    find_paths(graph, "end", ["end"])
  end

  defp find_paths_2(graph) do
    # We start from the end because each step will be prepended to the list of steps.
    # Like that, we get the paths in the right order.
    small_caves = Enum.filter(Map.keys(graph), fn cave ->
      small_cave?(cave) and cave !== "start" and cave !== "end"
    end)
    Stream.flat_map([nil | small_caves], &find_paths(graph, "end", ["end"], &1))
    |> Enum.uniq() # It's possible we made a path several times.
    # There has to be a way to directly prevent making duplicated paths in the first place,
    # But deduplicated after the fact will do.
  end

  defp find_paths(graph, start, so_far, twice \\ nil) do
    [last | _] = so_far
    Map.get(graph, last)
    |> Stream.filter(fn dest ->
      if small_cave?(dest) do
        cond do
          # We can't visit the starting node twice.
          dest === start -> false
          # Skip the small cave if we can't visit it twice.
          dest !== twice -> dest not in so_far
          # Skip the small cave if we have visited it twice already.
          Enum.count(so_far, &(&1 === dest)) >= 2 -> false
          # Else we can visit it.
          true -> true
        end
      else
        # We can always visit big caves.
        true
      end
    end)
    |> Enum.flat_map(fn dest ->
      if dest === "start" or dest === "end" do
        # We reached the end of the journey, return this particular path.
        # (Wrapped in a list because of the flat_map.)
        [[dest | so_far]]
      else
        find_paths(graph, start, [dest | so_far], twice)
      end
    end)
  end
end
