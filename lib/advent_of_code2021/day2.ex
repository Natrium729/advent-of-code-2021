defmodule AdventOfCode2021.Day2 do
  defmodule Submarine do
    defstruct pos: 0, depth: 0

    @spec follow_instructions(%Submarine{}, any) :: %Submarine{}
    def follow_instructions(submarine, instructions) do
      Enum.reduce(instructions, submarine, fn instruction, acc ->
        case instruction do
          [:forward, x] -> %{acc | pos: acc.pos + x}
          [:down, x] -> %{acc | depth: acc.depth + x}
          [:up, x] -> %{acc | depth: acc.depth - x}
        end
      end)
    end
  end

  defmodule SubmarineAim do
    defstruct pos: 0, depth: 0, aim: 0

    @spec follow_instructions(%SubmarineAim{}, any) :: %SubmarineAim{}
    def follow_instructions(submarine, instructions) do
      Enum.reduce(instructions, submarine, fn instruction, acc ->
        case instruction do
          [:forward, x] -> %{acc | pos: acc.pos + x, depth: acc.depth + acc.aim * x}
          [:down, x] -> %{acc | aim: acc.aim + x}
          [:up, x] -> %{acc | aim: acc.aim - x}
        end
      end)
    end
  end

  def solve(input) do
    input = parse_input(input)
    submarine1 = Submarine.follow_instructions(%Submarine{}, input)
    submarine2 = SubmarineAim.follow_instructions(%SubmarineAim{}, input)
    {submarine1.pos * submarine1.depth, submarine2.pos * submarine2.depth}
  end

  defp parse_input(input) do
    String.split(input)
    |> Stream.chunk_every(2)
    |> Enum.map(fn [dir, dist] -> [String.to_atom(dir), String.to_integer(dist)] end)
  end
end
