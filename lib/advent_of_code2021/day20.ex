defmodule AdventOfCode2021.Day20 do
  defmodule Image do
    @enforce_keys [:pixels, :x_min, :x_max, :y_min, :y_max, :out_pixel]
    defstruct [:pixels, :x_min, :x_max, :y_min, :y_max, :out_pixel]
    @type t :: %__MODULE__{
      pixels: MapSet.t,
      x_min: integer,
      x_max: integer,
      y_min: integer,
      y_max: integer,
      out_pixel: 0 | 1
    }

    @spec new(String.t) :: Image.t
    def new(input) do
      lines = String.split(input, "\n", trim: true)
      y_max = length(lines) - 1
      x_max = length(String.to_charlist(hd(lines))) - 1

      pixels = Stream.flat_map(lines, &String.to_charlist/1)

      # We only store light pixels in the map.
      pixels = for y <- 0..y_max, x <- 0..x_max do {x, y} end
      |> Stream.zip(pixels)
      |> Enum.reduce(MapSet.new(), fn {{x, y}, px}, acc ->
        if px === ?# do
          MapSet.put(acc, {x, y})
        else
          acc
        end
      end)

      %Image{
        pixels: pixels,
        x_min: 0,
        x_max: x_max,
        y_min: 0,
        y_max: y_max,
        out_pixel: 0, # Out of bounds pixels start dark.
      }
    end

    @spec get_pixel(Image.t, integer, integer) :: 0 | 1
    def get_pixel(image, x, y) do
      if x < image.x_min or x > image.x_max or y < image.y_min or y > image.y_max do
        # The pixel is out of bounds, use the outside value.
        image.out_pixel
      else
        if MapSet.member?(image.pixels, {x, y}), do: 1, else: 0
      end
    end

    @spec sample(Image.t, integer, integer) :: integer
    def sample(image, x, y) do
      # The order of dy and dx is important
      # since we have to concat adjacent pixels line by line.
      for dy <- -1..1, dx <- -1..1 do {dx, dy} end
      |> Stream.map(fn {dx, dy} -> get_pixel(image, x + dx, y + dy) end)
      |> Enum.join()
      # It would be more efficient to shift the bits and add them,
      # But joining into a string and parsing it will do.
      |> String.to_integer(2)
    end

    @spec enhance(Image.t, MapSet.t) :: Image.t
    def enhance(image, algorithm) do
      # We also have to enhance the pixels just outside of the image,
      # since their neighbours are inside the image.
      x_min = image.x_min - 1
      x_max = image.x_max + 1
      y_min = image.y_min - 1
      y_max = image.y_max + 1

      # Enhance each pixels
      pixels = for x <- x_min..x_max, y <- y_min..y_max do {x, y} end
      |> Enum.reduce(MapSet.new(), fn {x, y}, acc ->
        if MapSet.member?(algorithm, sample(image, x, y)) do
          MapSet.put(acc, {x, y})
        else
          acc
        end
      end)

      # Enhance out-of-bounds pixels.
      # Since the sample value can only have two values,
      # we could cache the result, but anyway.
      out_sample = if image.out_pixel === 1, do: 0b111111111, else: 0b000000000
      out_pixel = if MapSet.member?(algorithm, out_sample) do
        1
      else
        0
      end

      # Return the new image.
      %Image{
        pixels: pixels,
        x_min: x_min,
        x_max: x_max,
        y_min: y_min,
        y_max: y_max,
        out_pixel: out_pixel,
      }
    end
  end

  def solve(input) do
    {image_algorithm, input_image}  = parse_input(input)

    enhanced_twice = Enum.reduce(0..1, input_image, fn _, acc ->
      Image.enhance(acc, image_algorithm)
    end)

    # We'll assume the out-of-bounds pixels are dark, else the answer is infinity.
    answer1 = MapSet.size(enhanced_twice.pixels)

    enhanced_50 = Enum.reduce(0..49, input_image, fn _, acc ->
      Image.enhance(acc, image_algorithm)
    end)

    answer2 = MapSet.size(enhanced_50.pixels)

    {answer1, answer2}
  end

  @spec parse_input(String.t) :: {MapSet.t, Image.t}
  defp parse_input(input) do
    [image_algorithm, input_image] = String.split(input, "\n\n", trim: true)

    image_algorithm = Stream.iterate(0, &(&1 + 1))
    |> Stream.zip(String.to_charlist(image_algorithm))
    |> Enum.reduce(MapSet.new(), fn {i, pixel}, acc ->
      if pixel === ?# do
        MapSet.put(acc, i)
      else
        acc
      end
    end)

    {image_algorithm, Image.new(input_image)}
  end
end
