defmodule AdventOfCode2021.Day5 do
  @spec solve(String.t) :: {number, number}
  def solve(input) do
    lines = parse_input(input)

    answer1 = count_vents(lines)
    answer2 = count_vents(lines, all: true)

    {answer1, answer2}
  end

  @spec parse_input(String.t) :: [{number, number}]
  defp parse_input(input) do
    # We'll assume there are only simple line feeds.
    String.split(input, "\n", trim: true)
    |> Enum.map(fn line ->
      String.split(line, " -> ")
      |> Enum.map(fn coords ->
        String.split(coords, ",")
        |> Enum.map(&String.to_integer/1)
        |> List.to_tuple()
      end)
      |> List.to_tuple()
    end)
  end

  @spec count_vents([{number, number}], options: list) :: number
  defp count_vents(lines, options \\ []) do
    stream = if options[:all] do
      lines
    else
      Stream.filter(lines, fn {s, e} -> horizontal?(s, e) or vertical?(s, e) end)
    end
    Stream.flat_map(stream, fn {s, e} -> expand_line(s, e) end)
    |> Enum.reduce(%{}, fn coords, map -> add_vent(map, coords) end)
    |> Map.values()
    |> Stream.filter(&(&1 > 1))
    |> Enum.count()
  end

  defp horizontal?({_, y1}, {_, y2}) do
    if y1 === y2 do
      true
    else
      false
    end
  end

  defp vertical?({x1, _}, {x2, _}) do
    if x1 === x2 do
      true
    else
      false
    end
  end

  @spec expand_line({number, number}, {number, number}) :: [{number, number}]
  defp expand_line({x1, y1}, {x2, y2}) do
    cond do
      horizontal?({x1, y1}, {x2, y2}) -> Enum.map(x1..x2, fn x -> {x, y1} end)
      vertical?({x1, y1}, {x2, y2}) -> Enum.map(y1..y2, fn y -> {x1, y} end)
      true -> Enum.zip(x1..x2, y1..y2)
    end
  end

  defp add_vent(map, {x, y}) do
    {_, new_map} = Map.get_and_update(map, {x, y}, fn current ->
      if current === nil do
        {current, 1}
      else
        {current, current + 1}
      end
    end)
    new_map
  end
end
