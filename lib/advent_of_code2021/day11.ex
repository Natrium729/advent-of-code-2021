defmodule AdventOfCode2021.Day11 do
  def solve(input) do
    octopuses = parse_input(input)

    answer1 = steps(octopuses)
    |> Stream.drop(1) # To skip the initial state.
    |> Stream.take(100)
    |> Stream.map(fn step -> Enum.count(Map.values(step), &(&1 === 0)) end)
    |> Enum.sum()

    answer2 = steps(octopuses)
    |> Stream.drop(1)
    |> Stream.zip(Stream.iterate(1, &(&1 + 1)))
    |> Enum.find_value(fn {step, i} ->
      all = Stream.reject(step, fn {k, _} -> is_atom(k) end) # Discard the :width and :length fields.
      |> Enum.all?(fn {_, v} -> v === 0 end)
      if all do
        i
      else
        nil
      end
    end)

    {answer1, answer2}
  end

  defp parse_input(input) do
    lines = String.split(input, "\n", trim: true)
    height = length(lines)
    width = length(String.to_charlist(hd(lines)))

    Stream.flat_map(lines, fn line ->
      Enum.map(String.codepoints(line), &String.to_integer/1)
    end)
    |> Stream.zip(0 .. height * width - 1)
    |> Enum.reduce(%{height: height, width: width}, fn {val, i}, acc ->
      Map.put(acc, i, val)
    end)
  end

  defp get_at(octopuses, x, y) do
    if x < 0 or x > octopuses[:width] - 1 or y < 0 or y > octopuses[:height] - 1 do
      nil
    else
      i = x + y * octopuses[:width]
      octopuses[i]
    end
  end

  defp put_at(octopuses, x, y, value) do
    i = x + y * octopuses[:width]
    Map.put(octopuses, i, value)
  end

  defp neighbours(octopuses, x, y) do
    for dx <- -1..1,
        dy <- -1..1,
        # Found on day 15 that there is a bug here: the condition will always be true.
        # That means the octopus whose neighbours we are finding is itself considered a neighbour.
        # Luckily it still works!
        {dx, dy} !== 0,
        oct = get_at(octopuses, x + dx, y + dy),
        oct !== nil do
      {x + dx, y + dy}
    end
  end

  defp steps(octopuses) do
    Stream.iterate(octopuses, fn state ->
      for x <- 0 .. state[:width] - 1, y <- 0 .. state[:height] - 1 do
        {x, y}
      end
      |> Enum.reduce(state, fn {x, y}, acc ->
        update(acc, x, y)
      end)
      # And now set the flashed octopuses to 0.
      |> Enum.reduce(%{}, fn {i, v}, acc ->
        if v === :flash do
          Map.put(acc, i, 0)
        else
          Map.put(acc, i, v)
        end
      end)
    end)
  end

  defp update(octopuses, x, y) do
    oct = get_at(octopuses, x, y)
    if is_integer(oct) do
      if oct >= 9 do # Flash.
        octopuses = put_at(octopuses, x, y, :flash)
        Enum.reduce(neighbours(octopuses, x, y), octopuses, fn {xx, yy}, acc ->
          update(acc, xx, yy)
        end)
      else # Increment energy
        put_at(octopuses, x, y, oct + 1)
      end
    else # Already flashed, do nothing.
      octopuses
    end
  end
end
