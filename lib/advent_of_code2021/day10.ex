defmodule AdventOfCode2021.Day10 do
  def solve(input) do
    lines = parse_input(input)

    answer1 = Stream.map(lines, &corrupted_points/1)
    |> Enum.sum()

    autocomplete_scores = Stream.filter(lines, &(corrupted_points(&1) === 0))
    |> Stream.map(&closing_score/1)
    |> Enum.sort()
    # Advent of Code says that there'll always be an odd number of scores.
    answer2 = Enum.at(autocomplete_scores, floor(length(autocomplete_scores) / 2))

    {answer1, answer2}
  end

  defp parse_input(input) do
    String.split(input, "\n", trim: true)
  end

  defp parse_line(line) do
    String.to_charlist(line)
    # I guess we could have used a `reduce`, in the end.
    # Well, too late now.
    # (But a reduce would have to go up to the end,
    # while we can stop a soon as there's an error with a stream.
    |> Stream.scan({:ok, []}, fn bracket, acc ->
      case acc do
        {:ok, stack} -> push_bracket(stack, bracket)
        x -> x # If there's an error, we'll emit it up to the end.
      end
    end)
  end

  defp corrupted_points(line) do
    parse_line(line)
    |> Enum.find_value(0, fn
      {:ok, _} -> nil
      {:corrupted, bracket} -> corrupted_bracket_value(bracket)
    end)
  end

  # In most of the functions below,
  # we don't check if we have given them the right kind of character.
  # We'll assume the caller always does it.

  # Returns the opening bracket corresponding to the given closing one.
  defp opening_bracket(bracket) do
    case bracket do
      ?) -> ?(
      ?] -> ?[
      ?} -> ?{
      ?> -> ?<
    end
  end

  defp corrupted_bracket_value(bracket) do
    case bracket do
      ?( -> 3
      ?[ -> 57
      ?{ -> 1197
      ?< -> 25137
    end
  end

  defp open_bracket(stack, bracket) do
    # Opening a bracket will always succeed.
    case stack do
      # If the bracket is the same as the last opened one, increment it's counter.
      [{^bracket, n} | tail] -> {:ok, [{bracket, n + 1} | tail]}
      # Else push a brand new counter.
      _ -> {:ok, [{bracket, 1} | stack]}
    end
  end

  defp close_bracket(stack, bracket) do
    bracket = opening_bracket(bracket)
    case stack do
      # If the closed bracket was the last of it's kind, pop it.
      [{^bracket, 1} | tail] -> {:ok, tail}
      # Else decrement its counter.
      [{^bracket, n} | tail] -> {:ok, [{bracket, n - 1} | tail]}
      # But if the bracket does not match the last opened one, the line is corrupted.
      _ -> {:corrupted, bracket}
    end
  end

  defp push_bracket(stack, bracket) do
    case bracket do
      b when b in [?(, ?[, ?{, ?<] -> open_bracket(stack, bracket)
      b when b in [?), ?], ?}, ?>] -> close_bracket(stack, bracket)
    end
  end

  defp closing_bracket_value(bracket) do
    case bracket do
      ?( -> 1
      ?[ -> 2
      ?{ -> 3
      ?< -> 4
    end
  end

  defp closing_score(line) do
    {_, opened_brackets} = parse_line(line)
    |> Enum.at(-1) # Take the state when the line has been fully parsed.
    Enum.reduce(opened_brackets, 0, fn {bracket, n}, so_far ->
      Enum.reduce(1..n, so_far, fn _, acc ->
        acc * 5 + closing_bracket_value(bracket)
      end)
    end)
  end
end
