defmodule AdventOfCode2021.Day21 do
  defmodule Game do
    @enforce_keys [:pos_1, :pos_2]
    defstruct [:pos_1, :pos_2, score_1: 0, score_2: 0, next_player: 1, last_die: 0, rolled_count: 0]
    @type t :: %__MODULE__{
      pos_1: non_neg_integer,
      score_1: non_neg_integer,
      pos_2: non_neg_integer,
      score_2: non_neg_integer,
      next_player: 1 | 2,
      last_die: non_neg_integer,
      rolled_count: non_neg_integer,
    }

    @spec new(non_neg_integer, non_neg_integer) :: Game.t
    def new(pos_1, pos_2) do
      %Game{
        pos_1: pos_1,
        pos_2: pos_2,
      }
    end

    @spec play_turn(Game.t) :: Game.t
    def play_turn(game) do
      result =
        rem(game.last_die + 1, 100) +
        rem(game.last_die + 2, 100) +
        rem(game.last_die + 3, 100)
      game = %{
        game |
        last_die: rem(game.last_die + 3, 100),
        rolled_count: game.rolled_count + 3
      }

      if game.next_player === 1 do
        next_space = rem(game.pos_1 + result - 1, 10) + 1
        new_score = game.score_1 + next_space
        %{game | next_player: 2, pos_1: next_space, score_1: new_score}
      else
        next_space = rem(game.pos_2 + result - 1, 10) + 1
        new_score = game.score_2 + next_space
        %{game | next_player: 1, pos_2: next_space, score_2: new_score}
      end
    end

    @spec play_dirac_turn([Game.t]) :: [Game.t]
    def play_dirac_turn(games) do
      results = for d1 <- 1..3, d2 <- 1..3, d3 <- 1..3 do d1 + d2 + d3 end

      Enum.flat_map(games, fn game ->
        Stream.map(results, fn result ->
          if game.next_player === 1 do
            next_space = rem(game.pos_1 + result - 1, 10) + 1
            new_score = game.score_1 + next_space
            %{game | next_player: 2, pos_1: next_space, score_1: new_score}
          else
            next_space = rem(game.pos_2 + result - 1, 10) + 1
            new_score = game.score_2 + next_space
            %{game | next_player: 1, pos_2: next_space, score_2: new_score}
          end
        end)
      end)
    end
  end

  def solve(input) do
    game = parse_input(input)

    winning = Stream.iterate(game, &Game.play_turn/1)
    |> Enum.find(&(&1.score_1 >= 1000 or &1.score_2 >= 1000))

    answer1 = min(winning.score_1, winning.score_2) * winning.rolled_count

    # Playing each game is way too long. (I mean, it was obvious, right?)
    #
    # We could consider multiple games at the same time instead.
    # For example, rolling 1, 2, 3 on one turn is the same as rolling 3, 1, 2.
    # So instead of splitting each turn into 3^3 = 27 new turns,
    # We could only consider unique sets weighted by the number of combination they can form.

    # answer2 = Stream.iterate([game], &Game.play_dirac_turn/1)
    # |> Stream.map(fn games ->
    #   # Count each game with a winner, and discard them.
    #   Enum.reduce(games, {[], 0, 0}, fn game, {remaining, winner_1, winner_2} ->
    #     cond do
    #       game.score_1 >= 21 -> {remaining, winner_1 + 1, winner_2}
    #       game.score_2 >= 21 -> {remaining, winner_1, winner_2 + 1}
    #       true -> {[game | remaining], winner_1, winner_2}
    #     end
    #   end)
    # end)
    # |> Enum.find_value(fn {games, winner_1, winner_2} ->
    #   if games == [] do
    #     # All games came to an end.
    #     max(winner_1, winner_2)
    #   else
    #     nil
    #   end
    # end)
    answer2 = 0

    {answer1, answer2}
  end

  @spec parse_input(String.t) :: Game.t
  defp parse_input(input) do
    [line_1, line_2] = String.split(input, "\n", trim: true)

    <<"Player 1 starting position: ", pos_1::binary>> = line_1
    <<"Player 2 starting position: ", pos_2::binary>> = line_2

    Game.new(String.to_integer(pos_1), String.to_integer(pos_2))
  end
end
