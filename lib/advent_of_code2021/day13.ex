defmodule AdventOfCode2021.Day13 do
  def solve(input) do
    {dots, instructions} = parse_input(input)

    answer1 = folds(dots, instructions)
    |> Enum.at(0)
    |> Enum.count()

    answer2 = folds(dots, instructions)
    |> Enum.at(-1)
    |> dots_to_string()

    {answer1, answer2}
  end

  defp parse_input(input) do
    [dots, instructions] = String.split(input, "\n\n", trim: true)

    dots = String.split(dots, "\n", trim: true)
    |> Enum.map(fn line ->
      String.split(line, ",")
      |> Enum.map(&String.to_integer/1)
      |> List.to_tuple() end)
    |> MapSet.new()

    instructions = String.split(instructions, "\n", trim: true)
    |> Enum.map(fn line ->
      ["fold", "along", axis] = String.split(line)
      [orientation, value] = String.split(axis, "=")
      {String.to_atom(orientation), String.to_integer(value)}
    end)

    {dots, instructions}
  end

  # We could unify the two following functions
  # by storing the points in an %{:x, :y}` map instead of a tuple.

  defp fold_along(dots, :x, value) do
    {left, right} = Enum.split_with(dots, fn {x, _} -> x < value end)
    Enum.reduce(right, MapSet.new(left), fn {x, y}, acc ->
      MapSet.put(acc, {2 * value - x, y})
    end)
  end

  defp fold_along(dots, :y, value) do
    {top, bottom} = Enum.split_with(dots, fn {_, y} -> y < value end)
    Enum.reduce(bottom, MapSet.new(top), fn {x, y}, acc ->
      MapSet.put(acc, {x, 2 * value - y})
    end)
  end

  defp folds(dots, instructions) do
    Stream.scan(instructions, dots, fn {orientation, value}, acc ->
      fold_along(acc, orientation, value)
    end)
  end

  defp dots_to_string(dots) do
    {max_x, _} = Enum.max_by(dots, fn {x, _} -> x end)
    {_, max_y} = Enum.max_by(dots, fn {_, y} -> y end)
    for y <- 0..max_y, x <- 0..max_x do {x, y} end
    |> Enum.reduce("", fn {x, y}, acc ->
      acc = if x === 0 do
        acc <> "\n"
      else
        acc
      end
      if MapSet.member?(dots, {x, y}) do
        acc <> "#"
      else
        acc <> " "
      end
    end)
  end
end
