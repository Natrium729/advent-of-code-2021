defmodule AdventOfCode2021.Day9 do
  defmodule HeightMap do
    defstruct [:contents, :height, :width]
    @type t :: %__MODULE__{
      contents: [[non_neg_integer]],
      height: non_neg_integer,
      width: non_neg_integer
    }

    @spec new(String.t) :: HeightMap.t
    def new(contents) do
      contents = String.split(contents, "\n", trim: true)
      |> Enum.map(fn line ->
        String.codepoints(line)
        |> Enum.map(&String.to_integer/1)
      end)
      height = length(contents) # The number of lines.
      width = length(hd(contents)) # The number of cells in one line.
      %HeightMap{contents: contents, height: height, width: width}
    end

    @spec get(HeightMap.t, non_neg_integer, non_neg_integer) :: any
    def get(map, x, y) do
      # That's not efficient at all since we have walk the map to get an element.
      # Alas, there are no true arrays in Elixir.
      # (Well, apparently there are in Erlang and we could use that, or use a map...)
      Enum.at(map.contents, y)
      |> Enum.at(x)
    end

    # We could implement `Enumerable` on the `HeightMap`, but it seems like a pain.
    # It's way easier to just build a stream manually.
    def stream(map) do
      coords = Stream.flat_map(0 .. map.height - 1, fn y ->
        # For each y, we visit every x in turn.
        Stream.zip(0 .. map.width - 1, Stream.cycle([y]))
      end)
      # So it would seem `Stream` has no `flatten` method, only `flat_map`...
      Stream.zip(coords, List.flatten(map.contents))
      |> Stream.map(fn {{x, y}, cell} -> {x, y, cell} end)
    end

    @spec neighbours(HeightMap.t, non_neg_integer, non_neg_integer) :: [{non_neg_integer, non_neg_integer, non_neg_integer}]
    def neighbours(map, x, y) do
      # That's wholly inefficient since lists are linked lists in Elixir:
      # we have to walk the map for each neighbour.
      result = []
      result = if x > 0 do
        [{x - 1, y, HeightMap.get(map, x - 1, y)} | result]
      else
        result
      end
      result = if x < map.width - 1 do
        [{x + 1, y, get(map, x + 1, y)} | result]
      else
        result
      end
      result = if y > 0 do
        [{x, y - 1, get(map, x, y - 1)} | result]
      else
        result
      end
      result = if y < map.height - 1 do
        [{x, y + 1, get(map, x, y + 1)} | result]
      else
        result
      end
      result
    end

    def low_points(map) do
      stream(map)
      |> Stream.filter(fn {x, y, height} ->
        neighbours(map, x, y)
        |> Enum.all?(fn {_, _, other} -> height < other end)
      end)
    end

    # While the coordinates are enough,
    # we also include the height in the arguments
    # so that we don't need to walk the whole list.
    # We should just make sure it's really the height at the given coordinates.
    def higher_neighbours(map, x, y, height) do
      neighbours(map, x, y)
      |> Stream.filter(fn {_, _, other} -> height < other end)
    end

    def basin(map, x, y, height) do
      higher = higher_neighbours(map, x, y, height)
      |> Enum.filter(fn {_, _, height} -> height !== 9 end)
      if length(higher) === 0 do
        [{x, y, height}]
      else
        higher_higher = Enum.flat_map(higher, fn {x, y, height} ->
          basin(map, x, y, height)
        end)
        [{x, y, height} | higher_higher]
        |> Enum.uniq()
        # We have to remove duplicates because it's possible we visited the same cell twice
      end
    end

    def basins(map) do
      low_points(map)
      |> Stream.map(fn {x, y, height} -> basin(map, x, y, height) end)
    end
  end

  def solve(input) do
    map = parse_input(input)

    answer1 = HeightMap.low_points(map)
    |> Stream.map(fn {_, _, height} -> height + 1 end)
    |> Enum.sum()

    answer2 = HeightMap.basins(map)
    |> Stream.map(&length/1)
    |> Enum.sort(:desc)
    |> Stream.take(3)
    |> Enum.product()

    {answer1, answer2}
  end

  defp parse_input(input) do
    HeightMap.new(input)
  end
end
