defmodule AdventOfCode2021 do
  @moduledoc """
  Run with `mix run -e "AdventOfCode2021.solve(1)"`, replacing `1` with the day for which you want the answers.

  Using no argument in `solve` will show all the answers.
  """

  @solved_days Enum.concat([1..17, 20..21, [25]])

  def solve() do
    Enum.each(@solved_days, &solve/1)
  end


  def solve(day) do
    if day in @solved_days do
      input = File.read!("inputs/day#{day}.txt")
      {answer1, answer2} = apply(
        String.to_existing_atom("Elixir.AdventOfCode2021.Day#{day}"),
        :solve,
        [input]
      )
      IO.puts("Day #{day}.1: #{answer1}")
      IO.puts("Day #{day}.2: #{answer2}")
    else
      IO.puts("The day #{day} has not been solved.")
    end
  end
end
